
var SlideShow = function(duration) {
  setTimeout(function(){},duration);
  var imagenum = 1;

  setInterval(function(){
     $(".lockedBackgroundImage:nth-child(" + (imagenum+1) + ")").fadeIn(1000);
     $(".lockedBackgroundImage:not(:nth-child(" + (imagenum+1) + "))").fadeOut(1000);
     imagenum = ++imagenum % ($(".lockedBackgroundImage").length);
  }, duration);
}

$(document).ready(function() {
    $("h1>*").fadeOut(0);
    $('.lockedBackgroundImage:not(:first)').hide();


    //SlideShow(10000);
});

$("#main").on('scroll', function () {
    if ($("#main").scrollTop() == 0) {
        $("h1>*").fadeOut(500);
        $(".nameplate").fadeIn(500);
    } else {
        $(".nameplate").fadeOut(500);
        $("h1>*").fadeIn(500);
    }
});
