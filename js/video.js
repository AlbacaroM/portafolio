(function() {
	"use strict";
	
	var video = document.querySelector(".video1");
	var playButton = document.querySelector("#playVid");
	
	function init(){
	  console.log("from init");
	  //console.log(video.currentSrc);	
	  var toggleButton = document.querySelector("#playVid");
	  toggleButton.addEventListener("click", togglePlay, false);
	  }
	  
	function togglePlay(evt) {
	  console.log("From togglePlay()");
	  if(video.paused){
		  video.play();
		  //playButton.innerHTML = "pause";
		  //show pause
		  console.log(evt.currentTarget);
		  evt.currentTarget.innerHTML = "Pause";
		  
	  }else{
		  video.pause();
		  //show play
		  playButton.innerHTML = "play";
		  
	  }
	}
	
	
 window.addEventListener("load", init, false);
})();